from django.shortcuts import HttpResponse
import pandas as pd
from datetime import datetime
from sqlalchemy import create_engine
from .models import *
from .serializers import OverdueTaskSerializer
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny
from rest_framework.pagination import PageNumberPagination


custom_date_parser = lambda x: datetime.strptime(x, '%d.%m.%Y %H:%M:%S') if type(x) == str() else x
filial_parser = lambda x: x.split(' ')[-1:][0]


def upload_data(request):
    df = pd.read_csv('doc_tasks.csv',
                     parse_dates=['Дата выдачи', 'Дата выполнения (план)', 'Дата выполнения (факт)',
                                  'Дата', 'Дата завершения'],
                     date_parser=custom_date_parser)
    df.columns = ['id', 'doc_type', 'obj', 'organization', 'author', 'executor', 'task', 'issue_date',
                  'execute_date_plan', 'execute_date_fact', 'overdue_h', 'resolution', 'auto_done', 'template_contract',
                  'status', 'date', 'end_date', 'ended']
    df['author'] = df['author'].apply(filial_parser)
    engine = create_engine('postgresql://postgres:22323222@localhost:5432/osm')
    df.to_sql('app_overduetask', engine, if_exists='replace')
    return HttpResponse('Done!')


class OverdueTaskPagination(PageNumberPagination):
    page_size = 10


class OverdueTaskListAPIView(ListAPIView):
    serializer_class = OverdueTaskSerializer
    permission_classes = [AllowAny, ]
    pagination_class = OverdueTaskPagination

    def get_queryset(self):
        queryset = OverdueTask.objects.all()
        filial = self.request.query_params.get('filial')
        if filial is not None:
            queryset = queryset.filter(author=filial)
        return queryset
