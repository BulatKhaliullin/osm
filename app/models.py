from django.db import models


class OverdueTask(models.Model):
    id = models.IntegerField(unique=True, primary_key=True)
    doc_type = models.CharField(max_length=300, blank=True, null=True)
    obj = models.CharField(max_length=300, blank=True, null=True)
    organization = models.CharField(max_length=300, blank=True, null=True)
    author = models.CharField(max_length=300, blank=True, null=True)
    executor = models.CharField(max_length=300, blank=True, null=True)
    task = models.CharField(max_length=300, blank=True, null=True)
    issue_date = models.DateTimeField(blank=True, null=True)
    execute_date_plan = models.DateTimeField(blank=True, null=True)
    execute_date_fact = models.DateTimeField(blank=True, null=True)
    overdue_h = models.FloatField(blank=True, null=True)
    resolution = models.CharField(max_length=300, blank=True, null=True)
    auto_done = models.CharField(max_length=300, blank=True, null=True)
    template_contract = models.CharField(max_length=300, blank=True, null=True)
    status = models.CharField(max_length=300, blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    ended = models.CharField(max_length=300, blank=True, null=True)
