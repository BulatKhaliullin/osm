from rest_framework.serializers import ModelSerializer
from .models import OverdueTask


class OverdueTaskSerializer(ModelSerializer):
    class Meta:
        model = OverdueTask
        fields = '__all__'
