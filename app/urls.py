from django.urls import path
from .views import *


urlpatterns = [
    path('upload_data/', upload_data, name='upload_data'),
    path('tasks/', OverdueTaskListAPIView.as_view(), name='overdue_tasks_list')
]
